"""
 ============================================================================
 Name        : _PS5_bricolage.py
 Author      : Jakov Ivankovic
 Version     : 2019
 Copyright   : HEIA-FR Génie Electrique - PS5
 Description : PS5 Centrifuge control program
 ============================================================================
 """

import time
import RPi.GPIO as GPIO

from kivy.app import App
from kivy.uix.button import Button
from kivy.uix.boxlayout import BoxLayout
from kivy.uix.gridlayout import GridLayout
from kivy.uix.label import Label
from kivy.uix.slider import Slider
from kivy.core.window import Window
from kivy.uix.widget import Widget
from kivy.uix.popup import Popup
from kivy.clock import Clock



red = [1,0,0,1]
blue = [0.5,0.5,1,1]


#We are programming the GPIO by BCM pin numbers. example:(PIN35 as ‘GPIO19’)
GPIO.setmode(GPIO.BCM)

# GPIO 22 set up as input. It is pulled up to stop false signals
GPIO.setup(22, GPIO.IN, pull_up_down=GPIO.PUD_UP)

# GPIO 19 initialize as an output.
GPIO.setup(19,GPIO.OUT)
#GPIO19 as PWM output, with 100Hz frequency
pwm = GPIO.PWM(19,100)
#start pwm with dutycycle 0%
pwm.start(70)



#number of falling edges to count
NUM_CYCLES = 1000


##TEMPORARY TEST VARIABLES
frequency = 1000




##GLOBAL VARIABLES
speed_rpm_global = 0
hours_global_slider = 0
minutes_global_slider = 0
hours_global = 0
minutes_global = 0
runnig_machine_global = False



counter = 0  
frequency = 0
mean_frequency = 0
old_frequency = 0
measure_activ = True
timer_bool = True
start_time = 0
entry_time = 0
n_count = 0




print ("Starting the process \n"  )

########################################################################
class mainProg(App):
    #----------------------------------------------------------------------
    def build(self):
        """
#        timer = Timer(0,0)
        def read_frequency(instance):
            print("entered function read frequency")
            global runnig_machine_global
#            if runnig_machine_global:     #molto pericoloso poich nel caso in cui la macchina non gira e la variabile  vera si blocca tutto
            start = time.time()
            for impulse_count in range(NUM_CYCLES):
                if GPIO.add_event_detect(22, GPIO.FALLING):
                    counter = counter + 1
#               time.sleep(0.00015)
            duration = time.time() - start      #seconds to run for loop
            frequency = counter / duration
            print("Frequency : %s \n" %frequency)
            actual_RPM = frequency * 2.138
            print("Frequency : %s \n" %actual_RPM)
            self.speedLabel_actual.text = "%d" %(frequency * 2.138)
            print("exit function read frequency")
        pass
        """
        def myround(x, base =10):
            return base*round(x/base)
        
        
        
        def read_frequency(instance):
            global measure_activ
            global start_time
            global counter
            global timer_bool
            global entry_time
            global frequency
            global mean_frequency
            global n_count
            global old_frequency
            #time.sleep(0.00015)
            
            if measure_activ and timer_bool:
                start_time = time.time()
                timer_bool = False
                
            #check if the elapsed time is biger than the maximum period
            if (time.time() - entry_time) >= 0.000052: 
                counter += 1 
#            if counter>50 and measure_activ == True:
            if counter >= 2600:    
                duration = time.time() - start_time
#                frequency = (frequency + (counter / duration))/2
#                mean_frequency = int((counter / duration)+frequency)
                frequency = counter / duration
                mean_frequency = frequency + mean_frequency
#                print("Frequency : %s \n" %frequency)
                counter = 0
                start_time = 0
                measure_activ = True
                timer_bool = True
                self.speedLabel_actual.text = "%d" %myround(frequency *(60/28))
                n_count = n_count+1
#                print("frequency %d" %frequency)
##            print("Frequency : %s \n" %frequency)
#            if n_count >= 9:
#                mean_frequency = mean_frequency/10
##                print("Frequency : %s \n" %mean_frequency)
#                self.speedLabel_actual.text = "%d" %myround((mean_frequency+old_frequency)/2 *(60/28))
##                self.speedLabel_actual.text = "%d" %frequency
#                old_frequency = mean_frequency
#                n_count = 0
#                mean_frequency = 0
            entry_time= time.time()        

        """********************
        open_door function is going to control the solenoid on the main door of the machine. If the rotor is still
        running it will prevent the opening for security reasons.
        
        ********************"""
        def open_door(instance):
            print('My button door is pressed')



        """********************
        set_running_time
        ********************"""
        def set_running_time(instance):

            def on_value_hours(instance,hours):
                global hours_global_slider          # we want  to use the global variable not a local one
                hoursLabelValue.text = "%d" %hours
                hours_global_slider = hours
                print('Hours: %d' %hours_global_slider)

            def on_value_minutes(instance,minutes):
                global minutes_global_slider
                minutesLabelValue.text = "%d" %minutes
                minutes_global_slider = minutes
                print('Minutes: %d' %minutes_global_slider)

            layout = GridLayout(cols=2)
            popup = Popup(title='Select Running Time',content = layout,
                size_hint=(None, None), size=(400, 400))


            v_BoxLayout_pop1 = BoxLayout(padding=1, orientation="vertical")
            h_BoxLayout1 = BoxLayout(padding=1, orientation="horizontal")
            hoursLabelText = Label(text='Hours :',font_size=40)
            hoursLabelValue = Label(text='0',font_size=40)
            h_BoxLayout1.add_widget(hoursLabelText)
            h_BoxLayout1.add_widget(hoursLabelValue)
            v_BoxLayout_pop1.add_widget(h_BoxLayout1)
            sliderHours = Slider(padding=40,min=0, max=10, step=1)
            sliderHours.bind(value=on_value_hours)
            v_BoxLayout_pop1.add_widget(sliderHours)

            h_BoxLayout2 = BoxLayout(padding=1, orientation="horizontal")
            minutesLabelText = Label(text='Minutes',font_size=40)
            minutesLabelValue = Label(text='0',font_size=40)
            h_BoxLayout2.add_widget(minutesLabelText)
            h_BoxLayout2.add_widget(minutesLabelValue)
            v_BoxLayout_pop1.add_widget(h_BoxLayout2)
            sliderMinutes = Slider(padding=40,min=0, max=60, step=1)
            sliderMinutes.bind(value=on_value_minutes)
            v_BoxLayout_pop1.add_widget(sliderMinutes)
            h_BoxLayout = BoxLayout(padding=1, orientation="horizontal")
            btn_set=Button(text='SET',font_size=20)
            btn_exit=Button(text='EXIT',font_size=20)
            btn_set.bind(on_press = set_time_variables)
            btn_exit.bind(on_press = popup.dismiss)
            h_BoxLayout.add_widget(btn_set)
            h_BoxLayout.add_widget(btn_exit)
            v_BoxLayout_pop1.add_widget(h_BoxLayout)
            layout.add_widget(v_BoxLayout_pop1)
            popup.open()

##            v_layout_pop2 = BoxLayout(padding=1, orientation="vertical")
##            layout.add_widget(v_layout_pop2)




        def set_time_variables(instance):
            global hours_global_slider
            global minutes_global_slider
            global hours_global
            global minutes_global
            print('Setting variables %d' %hours_global_slider)
            hours_global =  hours_global_slider
            minutes_global = minutes_global_slider
            time_Label.text= '%d : %d'   % (hours_global, minutes_global)
            btn_time.text = "%d : %d : %d"   % (hours_global, minutes_global,00)
##            timer.countdown(hours_global, minutes_global)
##            instance.dismiss()
##            popup.dismiss() non funziona cosi poiche non é definito pero bisogna trovare una maniera per chiudere una volta che sono stati scelti i valori



        def on_press_start(instance):
            global running_machine_global
            running_machine_global = True
            #counter needs to be starded
            pass
 
            


        def on_value_rpm(instance,speed_rpm):
            global speed_rpm_global
            self.speedLabel.text = "%d" %speed_rpm
            speed_rpm_global = speed_rpm
            print('RPM: %d' %speed_rpm_global)

  

        self.speedControl = Slider(padding=10,min=0, max=5000, step=10)
        self.speedControl.bind(value=on_value_rpm)


        layout = GridLayout(cols=2)
        v_layout_1 = BoxLayout(padding=10, orientation="vertical")
        v_layout_1.add_widget(Label(text="ACTUAL SPEED",font_size=40))
        self.speedLabel_actual = Label(text='0000',font_size=40)
        self.speedLabel_actual.bind(on_value = read_frequency)
        v_layout_1.add_widget(self.speedLabel_actual)
        layout.add_widget(v_layout_1)

        v_layout_2 = BoxLayout(padding=40)
        btn_door = Button(text='OPEN DOOR', font_size=25, background_color = blue)
        btn_door.bind(on_press=read_frequency)                       #Calls the open_door function
        v_layout_2.add_widget(btn_door)
        layout.add_widget(v_layout_2)


        v_layout_3 = BoxLayout(padding=10, orientation="vertical")
        v_layout_3.add_widget(Label(text="SET SPEED",font_size=40))
        self.speedLabel = Label(text='0',font_size=40)
        v_layout_3.add_widget(self.speedLabel)
        layout.add_widget(v_layout_3)


        v_layout_4 = BoxLayout(padding=10, orientation="vertical")
        v_layout_4_1 = BoxLayout(padding=0, orientation="horizontal")
        v_layout_4_1.add_widget(Label(text="TIME :",font_size=50))
        time_Label = Label(text='00:00',font_size=40)
        v_layout_4_1.add_widget(time_Label)
        v_layout_4.add_widget(v_layout_4_1)
##        v_layout_4.add_widget(Label(text="TIME",font_size=50))
        btn_time = Button(text='00:00:00',font_size=50)
        btn_time.bind(on_press = set_running_time)
        v_layout_4.add_widget(btn_time)
##        v_layout_4.add_widget(Label(text='00:00:00',font_size=50))
        layout.add_widget(v_layout_4)


        v_layout_5 = BoxLayout(padding=40)
##        self.speedControl = Slider(min=0, max=5000, value=25)
        v_layout_5.add_widget(self.speedControl)
        layout.add_widget(v_layout_5)


        v_layout_6 = BoxLayout(padding=0, orientation="vertical")
        v_layout_6_1 = BoxLayout(padding=5, orientation="horizontal")       # cambiare il nome in h come horizontal
        btn_start = Button(text='START',font_size=30)
        btn_start.bind(on_press = on_press_start)
        v_layout_6_1.add_widget(btn_start)
        v_layout_6_1.add_widget(Button(text='STOP',background_color = red,font_size=30))
        v_layout_6.add_widget(v_layout_6_1)
        v_layout_6.add_widget(Button(text='OPTIONS'))
        layout.add_widget(v_layout_6)
        
        
        
        def update_Rpm(self):
            global frequency
#            self.speedLabel_actual.text = "%d" %(frequency *2.138)
            pass
        
        
        global runnig_machine_global
        if runnig_machine_global==True:
            pass
#            Clock.schedule_interval(read_frequency,4)
        
#        Clock.schedule_interval(update_Rpm,3)
        GPIO.add_event_detect(22, GPIO.FALLING,callback = read_frequency)


        return layout



#----------------------------------------------------------------------
if __name__ == "__main__":
    app = mainProg()
    app.run()

