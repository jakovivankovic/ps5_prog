"""
 ============================================================================
 Name        : _PS5_Centrifuge.py
 Author      : Jakov Ivankovic
 Version     : 2019
 Copyright   : HEIA-FR GÃ©nie Electrique - PS5
 Description : PS5 Centrifuge control program
 ============================================================================
 """

import time

from kivy.app import App
from kivy.uix.button import Button
from kivy.uix.boxlayout import BoxLayout
from kivy.uix.gridlayout import GridLayout
from kivy.uix.label import Label
from kivy.uix.slider import Slider
from kivy.core.window import Window
from kivy.uix.widget import Widget
from kivy.uix.popup import Popup
from kivy.clock import Clock
import RPi.GPIO as GPIO


#We are programming the GPIO by BCM pin numbers. example:(PIN35 as ‘GPIO19’)
GPIO.setwarnings(False)
GPIO.setmode(GPIO.BCM)

# GPIO 22 set up as input. It is pulled up to stop false signals
GPIO.setup(22, GPIO.IN, pull_up_down=GPIO.PUD_UP)

# GPIO 23 Output Door control
GPIO.setup(23, GPIO.OUT)
GPIO.output(23,0)

# GPIO 24 Power Supply control
GPIO.setup(24, GPIO.OUT)
GPIO.output(24,0)

# GPIO 19 initialize as an output.
GPIO.setup(19,GPIO.OUT)

#GPIO19 as PWM output, with 100Hz frequency
pwm = GPIO.PWM(19,100)
#start pwm with dutycycle 0%
pwm.start(0)



red = [1,0,0,1]
blue = [0.5,0.5,1,1]
green = [0.5,1,1,1]


##TEMPORARY TEST VARIABLES
frequency = 1000


##GLOBAL VARIABLES
set_rpm_global = 0
mes_rpm_global = 0
hours_global_slider = 0
minutes_global_slider = 0
hours_global = 0
minutes_global = 0
running_machine_global = False
time_seconds_global = 0



measure_activ = True           #true while measuring
timer_bool = True
start_time = 0
entry_time = 0
n_count = 0
counter = 0
frequency = 0
mean_frequency = 0
old_frequency = 0
pwm_value = 0
cons_actual = 0 






print ("Starting the process \n"  )

########################################################################
class mainProg(App):
    #----------------------------------------------------------------------
    def build(self):
        global running_machine_global
#        GPIO.output(23,0) #door closed
#        GPIO.output(24,0) # power supply off
        
        
        def myround(x, base =10):
            return base*round(x/base)

        def read_frequency(instance):
            global measure_activ
            global start_time
            global counter
            global timer_bool
            global entry_time
            global frequency
            global old_frequency
            global mes_rpm_global
            global running_machine_global
#            global mean_frequency
#            global n_count
            #time.sleep(0.00015)
            
            #starting the mesure process
            if measure_activ and timer_bool:
                start_time = time.time()
                timer_bool = False
            
            #check if the elapsed time is biger than the maximum period
            if (time.time() - entry_time) >= 0.00045: 
                counter += 1
#            elif (time.time() - entry_time) >= 0.0011:
#                self.speedLabel_actual.text = "%s" %0
#                print("0000")
#            if counter>50 and measure_activ == True:
            if counter >= 800:    
                duration = time.time() - start_time
#                frequency = (frequency + (counter / duration))/2
#                mean_frequency = int((counter / duration)+frequency)
                frequency = counter / duration
#                mean_frequency = frequency + mean_frequency
#                print("Frequency : %s \n" %frequency)
                counter = 0
                start_time = 0
                measure_activ = True
                timer_bool = True
                mes_rpm_global = frequency *(60/28)
                self.speedLabel_actual.text = "%d" %myround(mes_rpm_global)                   
#                n_count = n_count+1
#                print("frequency %d" %frequency)
##            print("Frequency : %s \n" %frequency)
#            if n_count >= 9:
#                mean_frequency = mean_frequency/10
##                print("Frequency : %s \n" %mean_frequency)
#                self.speedLabel_actual.text = "%d" %myround((mean_frequency+old_frequency)/2 *(60/28))
##                self.speedLabel_actual.text = "%d" %frequency
#                old_frequency = mean_frequency
#                n_count = 0
#                mean_frequency = 0
            if frequency < 70 and (running_machine_global==False):
                self.speedLabel_actual.text = "%s" %0
            entry_time= time.time() 
        
              

            

        """********************
        open_door function is going to control the solenoid on the main door of the machine. If the rotor is still
        running it will prevent the opening for security reasons.
        ********************"""
        def open_door(instance):
            global running_machine_global
            if running_machine_global == False:
                GPIO.output(23,1)
                print("door open ")
#                time.sleep(2)
#                GPIO.output(23,0)

        def close_door(instance):
           GPIO.output(23,0)  #it shuts down the solenoid conduction
           
        """********************
        set_running_time, this method is going to open a pop-up window whit two sliders, one for setting
        the hours the other for setting the minutes. If "Set" is pressed, the values will be saved
        ********************"""
        def set_running_time(instance):
                      
            def on_value_hours(instance,hours):
                global hours_global_slider          # we want  to use the global variable not a local one
                hoursLabelValue.text = "%d" %hours
                hours_global_slider = hours
#                print('Hours: %d' %hours_global_slider)

            def on_value_minutes(instance,minutes):
                global minutes_global_slider
                minutesLabelValue.text = "%d" %minutes
                minutes_global_slider = minutes
#                print('Minutes: %d' %minutes_global_slider)

            layout = GridLayout(cols=2)
            popup = Popup(title='Select Running Time',content = layout,
                size_hint=(None, None), size=(400, 400)) #size of te popup windows


            v_BoxLayout_pop1 = BoxLayout(padding=1, orientation="vertical")
            h_BoxLayout1 = BoxLayout(padding=1, orientation="horizontal")
            hoursLabelText = Label(text='Hours :',font_size=40)
            hoursLabelValue = Label(text='0',font_size=40)
            h_BoxLayout1.add_widget(hoursLabelText)
            h_BoxLayout1.add_widget(hoursLabelValue)
            v_BoxLayout_pop1.add_widget(h_BoxLayout1)
            sliderHours = Slider(padding=40,min=0, max=10, step=1)
            sliderHours.bind(value=on_value_hours)
            v_BoxLayout_pop1.add_widget(sliderHours)

            h_BoxLayout2 = BoxLayout(padding=1, orientation="horizontal")
            minutesLabelText = Label(text='Minutes',font_size=40)
            minutesLabelValue = Label(text='0',font_size=40)
            h_BoxLayout2.add_widget(minutesLabelText)
            h_BoxLayout2.add_widget(minutesLabelValue)
            v_BoxLayout_pop1.add_widget(h_BoxLayout2)
            sliderMinutes = Slider(padding=40,min=0, max=60, step=1)
            sliderMinutes.bind(value=on_value_minutes)
            v_BoxLayout_pop1.add_widget(sliderMinutes)
            h_BoxLayout = BoxLayout(padding=1, orientation="horizontal")
            btn_set=Button(text='SET',font_size=20)
            btn_exit=Button(text='EXIT',font_size=20)
            btn_set.bind(on_press = set_time_variables)
            btn_exit.bind(on_press = popup.dismiss)
            h_BoxLayout.add_widget(btn_set)
            h_BoxLayout.add_widget(btn_exit)
            v_BoxLayout_pop1.add_widget(h_BoxLayout)
            layout.add_widget(v_BoxLayout_pop1)
            popup.open()

##            v_layout_pop2 = BoxLayout(padding=1, orientation="vertical")
##            layout.add_widget(v_layout_pop2)



        def set_time_variables(instance):
            #mettere un controllo, se la macchina sta andando allora non cambiare le funzioni
            global hours_global_slider
            global minutes_global_slider
            global hours_global
            global minutes_global
            global time_seconds_global 
            print('Setting variables %d' %hours_global_slider)
            hours_global =  hours_global_slider
            minutes_global = minutes_global_slider
            time_seconds_global = int((hours_global * 60 * 60) + (minutes_global * 60))
            timer1 = '{:02d}:{:02d}:{:02d}'.format(int(hours_global) ,int(minutes_global), 00)
            btn_time.text = "%s"   % (timer1)
            timer2 = '{:02d}h:{:02d}m'.format(int(hours_global) ,int(minutes_global))
            time_Label.text= '%s'   % (timer2)
            
            
        def on_press_start(instance):
            global running_machine_global
            global time_seconds_global
            global cons_actual
            if (time_seconds_global>0):
                cons_actual = 0
                GPIO.output(24,1) #power supply on
                running_machine_global = True


        def on_press_stop(instance):
            global running_machine_global
            global time_seconds_global
            global pwm_value
            running_machine_global = False            
            timer1 = '{:02d}:{:02d}:{:02d}'.format(0,0,0)
            btn_time.text = "%s"   % (timer1)
            timer2 = '{:02d}h:{:02d}m'.format(0,0)
            time_Label.text= '%s'   % (timer2)
            time_seconds_global = 0
            pwm_value = 0
            pwm.start(0)
            GPIO.output(24,0) #power supply off
        
            
                        
        def on_value_rpm(instance,speed_rpm):
            global running_machine_global
            if running_machine_global == False:              #can't change speed if the mascine is running                           
                global set_rpm_global
                self.speedLabel.text = "%d" %speed_rpm
                set_rpm_global = speed_rpm
#            print('RPM: %d' %speed_rpm_global)

  
        self.speedControl = Slider(padding=10,min=100, max=4000, step=10)
        self.speedControl.bind(value=on_value_rpm)


        layout = GridLayout(cols=2)
        v_layout_1 = BoxLayout(padding=10, orientation="vertical")
        v_layout_1.add_widget(Label(text="ACTUAL SPEED [RPM]",font_size=35))
        self.speedLabel_actual = Label(text='0000',font_size=40)
        self.speedLabel_actual.bind(on_value = read_frequency)
        v_layout_1.add_widget(self.speedLabel_actual)
        layout.add_widget(v_layout_1)

        v_layout_2 = BoxLayout(padding=40)
        btn_door = Button(text='OPEN DOOR', font_size=25, background_color = blue)
        btn_door.bind(on_press=open_door)                       #Calls the open_door function
        btn_door.bind(on_release=close_door) 
        v_layout_2.add_widget(btn_door)
        layout.add_widget(v_layout_2)


        v_layout_3 = BoxLayout(padding=10, orientation="vertical")
        v_layout_3.add_widget(Label(text="SET SPEED [RPM]",font_size=35))
        self.speedLabel = Label(text='100',font_size=40)
        v_layout_3.add_widget(self.speedLabel)
        layout.add_widget(v_layout_3)


        v_layout_4 = BoxLayout(padding=10, orientation="vertical")
        v_layout_4_1 = BoxLayout(padding=0, orientation="horizontal")
        v_layout_4_1.add_widget(Label(text="TIME :",font_size=50))
        time_Label = Label(text='00h:00m',font_size=40)
        v_layout_4_1.add_widget(time_Label)
        v_layout_4.add_widget(v_layout_4_1)
##        v_layout_4.add_widget(Label(text="TIME",font_size=50))
        btn_time = Button(text='00:00:00',font_size=50)
        btn_time.bind(on_press = set_running_time)
        v_layout_4.add_widget(btn_time)
##        v_layout_4.add_widget(Label(text='00:00:00',font_size=50))
        layout.add_widget(v_layout_4)


        v_layout_5 = BoxLayout(padding=40)
##        self.speedControl = Slider(min=0, max=5000, value=25)
        v_layout_5.add_widget(self.speedControl)
        layout.add_widget(v_layout_5)


        v_layout_6 = BoxLayout(padding=0, orientation="vertical")
        v_layout_6_1 = BoxLayout(padding=5, orientation="horizontal")       # cambiare il nome in h come horizont

        btn_start = Button(text='START',font_size=30,background_color = blue)
        btn_start.bind(on_press = on_press_start)
        v_layout_6_1.add_widget(btn_start)
        btn_stop = Button(text='STOP',background_color = red,font_size=30)
        btn_stop.bind(on_press = on_press_stop)
        v_layout_6_1.add_widget(btn_stop)
        v_layout_6.add_widget(v_layout_6_1)
        v_layout_6.add_widget(Button(text='OPTIONS'))
        layout.add_widget(v_layout_6)
        
        
        
        def timer_count(self):
            global running_machine_global
            global time_seconds_global
            global pwm
            if running_machine_global == True:
                if(time_seconds_global == 0): on_press_stop(self)
                if (time_seconds_global/3600 > 1):          # si puo fare diversamente
                    hours = time_seconds_global/3600 %60
                    mins  = time_seconds_global/60 % 60
                    secs  = time_seconds_global %60
                    #mins, secs = divmod(time_seconds_global, 60)
                else:
                    hours = 0
                    mins, secs = divmod(time_seconds_global, 60)
                timer = '{:02d}:{:02d}:{:02d}'.format(int(hours) ,int(mins), int(secs))
                btn_time.text = "%s" % (timer)
                time_seconds_global -= 1           
                pass


##        global running_machine_global
##        if running_machine_global == True : 
##            Clock.schedule_interval(timer_count,1)
                       
        def reg_p(instance):
            global running_machine_global
            global pwm_value
            
#            global pwm_value
            if running_machine_global == True:
                global cons_actual
                global set_rpm_global 
                global mes_rpm_global
                if (set_rpm_global-100) > cons_actual :
                    cons_actual += 100
                else:
                    cons_actual = set_rpm_global
                error = cons_actual-mes_rpm_global
                
                 
#                if error < 0:
#                    pwm_value += 0.5*(100/4000)*error # 1/200 = 0.2*(100/4000)
                
#                if error > 700:
#                    pwm_value += 0.18/6*(100/4000)*error #0.18
#                if (error <= 0) :
#                    pwm_value += 0.6/4*(100/4000)*error #0.6
#                elif (error < 300) and (mes_rpm_global < 300): # starting condition
#                    pwm_value += 0.25/7*(100/4000)*error #0.25
#                else:
#                    pwm_value += 0.1/7*(100/4000)*error #0.1
                Kp = 0.1/10*(100/4000)
                pwm_value += Kp*error #0.1                   
                pwm.start(max(0,min(100,pwm_value)))
#                print("pwm_value %f" %max(0,min(100,pwm_value)))
#                print("error %f" %error)
                export_value = '{:02f},{:02f},{:02f}'.format(float(max(0,min(100,pwm_value))) ,float(error), int(mes_rpm_global))
#                print("%f, %f, %d" %max(0,min(100,pwm_value)), %error, %mes_rpm_global)
                print("%s" % (export_value))

            
        Clock.schedule_interval(timer_count,1)    
        GPIO.add_event_detect(22, GPIO.FALLING,callback = read_frequency)
        Clock.schedule_interval(reg_p,1)
        
        return layout




 


#----------------------------------------------------------------------
if __name__ == "__main__":
    app = mainProg()
    app.run()

