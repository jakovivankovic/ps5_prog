
import time 
import RPi.GPIO as GPIO  
GPIO.setmode(GPIO.BCM)

NUM_CYCLES = 1000

  
# GPIO 22 set up as input. It is pulled up to stop false signals  
#GPIO.setup(22, GPIO.IN, pull_up_down=GPIO.PUD_UP)
GPIO.setup(22, GPIO.IN, pull_up_down=GPIO.PUD_UP)
  

  
  
counter = 0  
frequency = 0
measure_activ = False
timer_bool = False
start_time=0


def read_frequency(instance):
    global measure_activ
    global start_time
    global counter
    global timer_bool
#    time.sleep(0.0003) 
    if measure_activ and timer_bool:
        start_time = time.time()
        timer_bool = False
    if counter > 1000 and measure_activ == True:
        duration = time.time() - start_time
        frequency = counter / duration
        print("Frequency : %s \n" %(frequency))
        counter = 0
        start_time = 0
        measure_activ = True
        timer_bool = True
    #print("Counter %d" %counter)
    counter = counter+1
            
        
GPIO.add_event_detect(22, GPIO.FALLING,callback = read_frequency)
measure_activ = True
timer_bool = True
  
#  
#while True:
#    
#    time.sleep(2)
#    counter = 0
#    start = time.time()
#    for impulse_count in range(NUM_CYCLES):
#        #GPIO.add_event_detect(22, GPIO.FALLING,callback = my_callback, bouncetime = 300)
#        #time.sleep(0.00015)
#        pass
#    duration = time.time() - start      #seconds to run for loop
#    frequency = counter / duration
#    print("Frequency : %s \n" %frequency)
#    
    
    
        #print("stopping time %s \n" %time.time())
   

#except KeyboardInterrupt:  
#  GPIO.cleanup()       # clean up GPIO on CTRL+C exit  
#GPIO.cleanup()           # clean up GPIO on normal exit