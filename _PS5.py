#jakov Ivankovic

from kivy.app import App
from kivy.uix.button import Button
from kivy.uix.boxlayout import BoxLayout
from kivy.uix.gridlayout import GridLayout
from kivy.uix.label     import Label
from kivy.uix.slider import Slider

from kivy.core.window import Window
from kivy.uix.widget import Widget
 




red = [1,0,0,1]

########################################################################
class mainProg(App): 
    #----------------------------------------------------------------------
    def build(self):
        
        def open_door(instance):
            print('My button door is pressed')

                
        def on_value(instance,speed_rpm):
            self.speedLabel.text = "%d" %speed_rpm
            

        

        self.speedControl = Slider(padding=10,min=0, max=5000, step=10)
        self.speedControl.bind(value=on_value)


        
    
        layout = GridLayout(cols=2)
        v_layout_1 = BoxLayout(padding=10, orientation="vertical")
        v_layout_1.add_widget(Label(text="ACTUAL SPEED",font_size=40))
        v_layout_1.add_widget(Label(text='0000 RPM',font_size=40))
        layout.add_widget(v_layout_1)

        v_layout_2 = BoxLayout(padding=40)
        btn_door = Button(text='OPEN DOOR', font_size=25)
        btn_door.bind(on_press=open_door)                       #Calls the callback1 function
        v_layout_2.add_widget(btn_door)
        layout.add_widget(v_layout_2)


        v_layout_3 = BoxLayout(padding=10, orientation="vertical")
        v_layout_3.add_widget(Label(text="SET SPEED",font_size=40))
        self.speedLabel = Label(text='0',font_size=40)
        v_layout_3.add_widget(self.speedLabel)
        layout.add_widget(v_layout_3)


        v_layout_4 = BoxLayout(padding=10, orientation="vertical")
        v_layout_4.add_widget(Label(text="TIME",font_size=50))
        v_layout_4.add_widget(Label(text='00:00',font_size=50))
        layout.add_widget(v_layout_4)


        v_layout_5 = BoxLayout(padding=40)
##        self.speedControl = Slider(min=0, max=5000, value=25)
        v_layout_5.add_widget(self.speedControl)
        layout.add_widget(v_layout_5)


        v_layout_6 = BoxLayout(padding=10, orientation="vertical")
        v_layout_6_1 = BoxLayout(padding=5, orientation="horizontal")
        v_layout_6_1.add_widget(Button(text='START',font_size=30))
        v_layout_6_1.add_widget(Button(text='STOP',background_color = red,font_size=30))
        
        v_layout_6.add_widget(v_layout_6_1)
        v_layout_6.add_widget(Button(text='OPTIONS'))
        layout.add_widget(v_layout_6)




        
        return layout



#----------------------------------------------------------------------
if __name__ == "__main__":
    app = mainProg()
    app.run()
