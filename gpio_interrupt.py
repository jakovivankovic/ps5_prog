
import time 
import RPi.GPIO as GPIO  
GPIO.setmode(GPIO.BCM)

NUM_CYCLES = 1000

start = time.time()
  
# GPIO 22 set up as input. It is pulled up to stop false signals  
GPIO.setup(22, GPIO.IN, pull_up_down=GPIO.PUD_UP)  
  
print ("Make sure you have a button connected so that when pressed")  
print ("it will connect GPIO port 23 (pin 16) to GND (pin 6)\n")  
##raw_input("Press Enter when ready\n>")  
  
print ("Waiting for falling edge on port 23")  
# now the program will do nothing until the signal on port 23   
# starts to fall towards zero. This is why we used the pullup  
# to keep the signal high and prevent a false interrupt  
  
print ("During this waiting time, your computer is not" )  
print ("wasting resources by polling for a button press.\n\n")  
print ("Starting the process \n"  )
#counter = 1
#while True:  
 #   GPIO.wait_for_edge(22, GPIO.RISING)
  #  counter += 1
    #print("Edge detected")
   # print ("Counter Value = %s \n" % counter)
  
#print("starting time: %s \n" %time.time())
  
  
counter = 0  
frequency = 0
  
  
while True:
    
    #time.sleep(2)
    start = time.time()
    for impulse_count in range(NUM_CYCLES):
        GPIO.wait_for_edge(22, GPIO.FALLING)
        time.sleep(0.00015)
    duration = time.time() - start      #seconds to run for loop
    frequency = float(NUM_CYCLES / duration)
    print("Frequency : %s \n" %frequency)
    print("RPM: %d\n" %(frequency*60/28))
    
    
    
        #print("stopping time %s \n" %time.time())
   

#except KeyboardInterrupt:  
#  GPIO.cleanup()       # clean up GPIO on CTRL+C exit  
#GPIO.cleanup()           # clean up GPIO on normal exit
